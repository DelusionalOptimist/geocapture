# GeoCapture

## For testing out monitoring
To try it out you need vagrant and virtualbox. Once you have them installed, run:
```
vagrant up
```
The grafana dashboard would be exposed on port 3000 for geocapture-demo-vm

## For testing out filtering
You need to use Vagrantfile-2
```
mv Vagrantfile Vagrantfile.bak
mv Vagrantfile-2 Vagrantfile
vagrant up
```
