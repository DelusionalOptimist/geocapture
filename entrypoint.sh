#!/bin/env bash

mount -t debugfs none /sys/kernel/debug
python3 -m "http.server" 8080 > /dev/null 2>&1 &
python3 main.py
