FROM archlinux:latest

RUN pacman --noconfirm -Sy
RUN pacman -S --noconfirm bcc bcc-tools python-bcc python-pip

WORKDIR /app

ADD ./requirements.txt .
RUN pip3 install -r requirements.txt

ADD . .


CMD ["bash", "./entrypoint.sh"]
