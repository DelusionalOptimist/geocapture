build:
	docker build -t geocapture-ebpf-daemon:latest .

run:
	docker run -p 8080:8080 -p 8000:8000 --network=host --privileged -it --rm --name geocapture-ebpf-daemon \
	--env NETWORK_INTERFACE=eth0 \
	-v /lib/modules:/lib/modules:ro \
	-v /usr/src:/usr/src:ro \
	-v /etc/localtime:/etc/localtime:ro \
	-v ${PWD}/daemon.c:/app/daemon.c:ro \
	-v /sys/fs/bpf:/sys/fs/bpf \
	geocapture-ebpf-daemon:latest

build-run: build run

run-host:
	docker run --network host --privileged -it --rm --name geocapture-ebpf-daemon \
	-v /lib/modules:/lib/modules:ro \
	-v /usr/src:/usr/src:ro \
	-v /etc/localtime:/etc/localtime:ro \
	geocapture-ebpf-daemon:latest

build-run-host: build run-host
